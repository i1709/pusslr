# pusslr

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

A puzzle (e.g. Advent of Code puzzles) solving library written in Rust.

Our goal with this library is to:
* learn `rust`
* gather a collection of re-usable code to solve puzzles

Usage:

```
cargo run aoc_2022-1_1
```

## Roadmap

- [ ] DOCS: document code
- [ ] CLI: interactive puzzle selector
- [ ] CLI: show licensing [info](https://opensource.org/licenses/GPL-3.0)
- [x] ARCH: implement a `Puzzle` trait (?) with a DB init and a keygen methods
- [x] ARCH: Remove the dependency to the `aoc` module.

## Licensing

The entire repository is licensed under `GPL-3.0-only`: see `LICENSE` file.

## Code of Conduct

See the [Code of Conduct](CODE_OF_CONDUCT.md).

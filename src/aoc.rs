// pusslr - a code puzzle solving library
// Copyright (C) 2022 Nathan Lövsund
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;

use crate::puzzle::Puzzle;

pub struct AocPuzzle {
    pub input_path: String,
    pub id: String,
}

impl Puzzle for AocPuzzle {
    fn init_puzzle_db() -> HashMap<String, fn(&str) -> String> {
        let mut puzzle_db = HashMap::<String, fn(&str) -> String>::new();
        puzzle_db.insert("1_1".to_string(), day1_1);
        puzzle_db.insert("1_2".to_string(), day1_2);
        puzzle_db.insert("2_1".to_string(), day2_1);
        puzzle_db.insert("2_2".to_string(), day2_2);
        puzzle_db
    }
}

// TODO: Rename and move to another module
fn feed_list(input: &str, list: &mut Vec<u32>) {
    for line in input.lines() {
        if line.is_empty() {
            list.push(0);
        } else {
            // TODO: refactor in a generic function
            *list.last_mut().unwrap() += line.parse::<u32>().unwrap();
        }
    }
}

// TODO: Move to another module
fn find_max_from_list<T: std::cmp::Ord>(list: &[T]) -> Option<&T> {
    list.iter().max()
}

fn day1_1(input: &str) -> String {
    let mut calories: Vec<u32> = vec![0];
    feed_list(input, &mut calories);
    find_max_from_list(&calories).unwrap().to_string()
}

fn day1_2(input: &str) -> String {
    let mut calories: Vec<u32> = vec![0];
    feed_list(input, &mut calories);
    calories.sort();
    calories.reverse();
    (calories[0] + calories[1] + calories[2]).to_string()
}

struct RockPaperScissorMatch {
    p2_score: usize,
}

impl RockPaperScissorMatch {
    fn new() -> RockPaperScissorMatch {
        RockPaperScissorMatch { p2_score: 0 }
    }

    fn play_round(&mut self, p1_choice: &str, p2_choice: &str) {
        let p1_move = RockPaperScissorMatch::choice_to_move_guess(p1_choice);
        let p2_move = RockPaperScissorMatch::choice_to_move_guess(p2_choice);
        RockPaperScissorMatch::score_move(p2_move, &mut self.p2_score);
        self.compute_outcome(p1_move, p2_move);
    }

    fn score_move(game_move: &str, score: &mut usize) {
        if game_move == "Rock" {
            *score += 1;
        } else if game_move == "Paper" {
            *score += 2;
        } else if game_move == "Scissor" {
            *score += 3;
        }
    }

    fn compute_outcome(&mut self, p1_move: &str, p2_move: &str) {
        if p1_move == p2_move {
            self.p2_score += 3;
        } else if p1_move == "Rock" && p2_move == "Paper"
            || p1_move == "Paper" && p2_move == "Scissor"
            || p1_move == "Scissor" && p2_move == "Rock"
        {
            self.p2_wins();
        }
    }

    fn p2_wins(&mut self) {
        self.p2_score += 6;
    }

    fn choice_to_move_guess(choice: &str) -> &'static str {
        if choice == "A" || choice == "X" {
            "Rock"
        } else if choice == "B" || choice == "Y" {
            "Paper"
        } else {
            "Scissor"
        }
    }

    fn translate_p2_choice(p2_advice: &str, p1_choice: &str) -> &'static str {
        if p2_advice == "X" {
            RockPaperScissorMatch::lose_to(p1_choice)
        } else if p2_advice == "Y" {
            RockPaperScissorMatch::draw_to(p1_choice)
        } else {
            RockPaperScissorMatch::win_to(p1_choice)
        }
    }

    fn lose_to(p1_choice: &str) -> &'static str {
        if p1_choice == "A" {
            "C"
        } else if p1_choice == "B" {
            "A"
        } else {
            "B"
        }
    }

    fn draw_to(p1_choice: &str) -> &'static str {
        if p1_choice == "A" {
            "A"
        } else if p1_choice == "B" {
            "B"
        } else {
            "C"
        }
    }

    fn win_to(p1_choice: &str) -> &'static str {
        if p1_choice == "A" {
            "B"
        } else if p1_choice == "B" {
            "C"
        } else {
            "A"
        }
    }
}

fn day2_1(input: &str) -> String {
    let mut tournament = RockPaperScissorMatch::new();
    for line in input.lines() {
        let mut line = line.split(' ');
        tournament.play_round(line.next().unwrap(), line.next().unwrap());
    }
    tournament.p2_score.to_string()
}

fn day2_2(input: &str) -> String {
    let mut tournament = RockPaperScissorMatch::new();
    for line in input.lines() {
        let mut line = line.split(' ');
        let p1_choice = line.next().unwrap();
        let p2_advice = line.next().unwrap();
        let p2_choice = RockPaperScissorMatch::translate_p2_choice(p2_advice, p1_choice);
        tournament.play_round(p1_choice, p2_choice);
    }
    tournament.p2_score.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const DAY_1_TEST_INPUT: &str = "\
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

    #[test]
    fn test_solution_day_1_1() {
        assert_eq!("24000", day1_1(DAY_1_TEST_INPUT));
    }

    #[test]
    fn test_solution_day_1_2() {
        assert_eq!("45000", day1_2(DAY_1_TEST_INPUT));
    }

    const DAY_2_TEST_INPUT: &str = "\
A Y
B X
C Z";

    #[test]
    fn test_solution_day_2_1() {
        assert_eq!("15", day2_1(DAY_2_TEST_INPUT));
    }

    #[test]
    fn test_solution_day_2_2() {
        assert_eq!("12", day2_2(DAY_2_TEST_INPUT));
    }
}

// pusslr - a code puzzle solving library
// Copyright (C) 2022 Nathan Lövsund
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub mod aoc;
pub mod puzzle;
pub mod utils;

use crate::puzzle::Puzzle;
use std::error::Error;
use std::fs;

// TODO: Works only for AoC puzzle, needs refactoring
pub fn run(puzzle: puzzle::PuzzleProvider) -> Result<String, Box<dyn Error>> {
    let input = fs::read_to_string(puzzle.input_path)?;

    let solution = solver(&input, &puzzle.id);

    println!("Problem solution: {solution}");

    Ok(solution)
}

fn solver(input: &str, key: &str) -> String {
    let puzzle_db = aoc::AocPuzzle::init_puzzle_db();

    match puzzle_db.get(&key.to_string()) {
        Some(solution) => solution(input),
        None => panic!("Puzzle not found!"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solution_day_1_1() {
        let test_input = "\
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

        assert_eq!("24000", solver(test_input, "1_1",));
    }
}

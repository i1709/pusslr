// pusslr - a code puzzle solving library
// Copyright (C) 2022 Nathan Lövsund
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;

use crate::utils::{extract_argument, form_input_path};

pub trait Puzzle {
    fn init_puzzle_db() -> HashMap<String, fn(&str) -> String>;
}

pub struct PuzzleProvider {
    pub input_path: String,
    pub id: String,
}

impl PuzzleProvider {
    pub fn build(args: &[String]) -> Result<PuzzleProvider, &'static str> {
        if args.len() < 2 {
            return Err("missing arguments: cargo run aoc_YEAR-DAY_PART");
        }
        let puzzle_id = args[1].clone();
        let mut puzzle_id = puzzle_id.split('-');

        let location = extract_argument(&mut puzzle_id).unwrap().to_string();

        let puzzle_num = extract_argument(&mut puzzle_id).unwrap();

        let input_path = form_input_path(location, puzzle_num);

        Ok(PuzzleProvider {
            input_path,
            id: puzzle_num.to_string(),
        })
    }
}

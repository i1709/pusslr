// pusslr - a code puzzle solving library
// Copyright (C) 2022 Nathan Lövsund
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::str::Split;

pub fn form_input_path(location: String, id: &str) -> String {
    let path = "data/input/".to_string() + &location + "/";
    let id = id.split('_').next().unwrap();
    if id.parse::<u8>().unwrap() < 10 {
        path + "0" + id + ".txt"
    } else {
        path + id + ".txt"
    }
}

pub fn extract_argument<'a>(arguments: &'a mut Split<char>) -> Result<&'a str, &'a str> {
    if let Some(value) = arguments.next() {
        Ok(value)
    } else {
        // TODO: refactor constant
        Err("malformed argument: cargo run puzzle_name-puzzle_number")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_input_path_formation() {
        let location: String = String::from("aoc_2022");
        let day = String::from("1");
        assert_eq!(
            "data/input/aoc_2022/01.txt",
            form_input_path(location, &day)
        )
    }

    #[test]
    fn test_argument_extractor_on_valid_input() {
        let args = "abc_XYXY-1_1";
        let mut args = args.split('-');
        assert_eq!(Ok("abc_XYXY"), extract_argument(&mut args));
        assert_eq!(Ok("1_1"), extract_argument(&mut args));
    }

    #[test]
    fn test_argument_extractor_on_invalid_input() {
        let args = "abc_XYXY";
        let mut args = args.split('-');
        assert_eq!(Ok("abc_XYXY"), extract_argument(&mut args));
        assert_eq!(
            Err("malformed argument: cargo run puzzle_name-puzzle_number"),
            extract_argument(&mut args)
        );
    }
}

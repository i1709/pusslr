use pusslr::puzzle::PuzzleProvider;

#[test]
fn aoc_2022_day1_1() {
    let input_path = "data/input/aoc_2022/01.txt".to_string();
    let id = "1_1".to_string();
    let solution = pusslr::run(PuzzleProvider { input_path, id }).unwrap();
    assert_eq!("66186", solution);
}

#[test]
fn aoc_2022_day1_2() {
    let input_path = "data/input/aoc_2022/01.txt".to_string();
    let id = "1_2".to_string();
    let solution = pusslr::run(PuzzleProvider { input_path, id }).unwrap();
    assert_eq!("196804", solution);
}

#[test]
fn aoc_2022_day2_1() {
    let input_path = "data/input/aoc_2022/02.txt".to_string();
    let id = "2_1".to_string();
    let solution = pusslr::run(PuzzleProvider { input_path, id }).unwrap();
    assert_eq!("15523", solution);
}

#[test]
fn aoc_2022_day2_2() {
    let input_path = "data/input/aoc_2022/02.txt".to_string();
    let id = "2_2".to_string();
    let solution = pusslr::run(PuzzleProvider { input_path, id }).unwrap();
    assert_eq!("15702", solution);
}
